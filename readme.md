## Commands for submodules management

### ADD submodule
` git submodule add <submodule_link> <submodule_directory> `

### UPDATE submodule
` git submodule update --remote --merge `

### CLONE submodule
` git clone —recurse-submodules <main_link> `

### REMOVE submodule

- ` git submodule deinit <submodule_name> `
- Remove submodule in .gitmodules
- ` git rm —cached <submodule_name> `
- ` rm -rf .git/modules/<submodule_name> `
- Remove submodule folder

## Commands for deploying on firebase

### SET UP TARGET
- `firebase target:apply hosting <target-name> <resource-name>`
By default, we will put the target-name and the resource-name, the same
- Add object in firebase.json's hosting array: 
` ,{
    "target": "target-name",
    "public": "static-files-directory",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ]
} `

### DEPLOY HOSTING
`firebase deploy --only hosting:<target-name>`
